package ru.tsc.panteleev.tm.exception.field;

public final class EmailExistsException extends AbstractFieldException {

    public EmailExistsException() {
        super("Error! This email already exists in the system...");
    }
    
}
