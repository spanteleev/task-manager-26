package ru.tsc.panteleev.tm.exception.field;

public final class LoginExistsException extends AbstractFieldException {

    public LoginExistsException() {
        super("Error! This login already exists in the system...");
    }
    
}
