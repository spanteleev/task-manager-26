package ru.tsc.panteleev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.model.Task;
import ru.tsc.panteleev.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Display task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @NotNull final Task task = getTaskService().findById(userId, id);
        showTask(task);
    }

}
