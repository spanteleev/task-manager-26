package ru.tsc.panteleev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.model.Project;
import java.util.Date;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description, @Nullable Date dateBegin, @Nullable Date dateEnd);

    Project updateByIndex(String userId, Integer index, String name, String description);

    Project updateById(String userId, String id, String name, String description);

    Project changeStatusByIndex(String userId, Integer index, Status status);

    Project changeStatusById(String userId, String id, Status status);

}
